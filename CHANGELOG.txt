
deadwood 6.x-1.x, 2009-xx-xx (development version)
----------------------------
- Changes (2009-08-06):
   * Replace messages to screen with writes to a log file.
   * Add .test as a file extension to convert and sort all extensions.
   * Rename 'files' key to to 'deadwood_files' in hook_requirements().
   * Removing trailing spaces in all files.
