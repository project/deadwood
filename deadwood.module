<?php

/**
 * @file
 * Generate version upgrade code from 5.x to 6.x.
 *
 * Copyright 2008 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

module_load_include('inc', 'deadwood', 'deadwood');
module_load_include('inc', 'deadwood', 'deadwood.help');
module_load_include('inc', 'deadwood', 'deadwood.conversions');

/**
 * Implementation of hook_perm().
 */
function deadwood_perm() {
  return array('manage conversions');
}

/**
 * Implementation of hook_node_info().
 */
function deadwood_node_info() {
  $info = array();
  $info['deadwood_category'] = array(
    'name' => t('Deadwood category'),
    'module' => 'deadwood',
    'description' => 'Category for deadwood conversion items.',
    'title_label' => t('Deadwood category'),
    'body_label' => t('Description'),
  );
  $info['deadwood_item'] = array(
    'name' => t('Deadwood item'),
    'module' => 'deadwood',
    'description' => 'Item for a deadwood conversion category.',
    'title_label' => t('Deadwood item'),
    'body_label' => t('Description'),
  );
  return $info;
}

/**
 * Implementation of hook_menu().
 */
function deadwood_menu() {
  $items = array();

  // Settings related items.
  $items['admin/settings/deadwood'] = array(
    'title' => 'Module conversion',
    'description' => 'Configure the module conversion suite.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deadwood_settings'),
    'access arguments' => array('access administration pages')
    );
  $items['admin/settings/deadwood/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['admin/settings/deadwood/api'] = array(
    'title' => 'API changes',
    'description' => 'Configure default api changes to make.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deadwood_api_settings'),
    'access arguments' => array('access administration pages'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -9
  );

  // Conversion items.
  $items['admin/build/deadwood'] = array(
    'title' => 'Conversions',
    'description' => 'Convert module code from version 5.x to 6.x.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('deadwood_conversions'),
    'access arguments' => array('manage conversions')
  );

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function deadwood_theme() {
  return array(
    'deadwood_extensions_table' => array(
      'arguments' => array('form' => NULL)
    ),
    'deadwood_conversions_table' => array(
      'arguments' => array('form' => NULL)
    )
  );
}

/**
 * Implementation of settings form.
 */
function deadwood_settings() {
  $form = array();

  $path = file_directory_path();
  $form['deadwood_dir'] = array(
    '#title' => t('Module input directory'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('deadwood_dir', DEADWOOD_IN),
    '#description' => t('Directory beneath the file system path (!path) in which to upload 5.x module code. Default is deadwood.', array('!path' => $path)),
    '#size' => 30,
    '#maxlength' => 255,
    '#validate' => array('deadwood_validate_in_dir')
  );

  $form['goodwood_dir'] = array(
    '#title' => t('Module output directory'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('goodwood_dir', DEADWOOD_OUT),
    '#description' => t('Directory beneath the file system path (!path) in which to save the converted 6.x module code. Default is goodwood.', array('!path' => $path)),
    '#size' => 30,
    '#maxlength' => 255,
    '#validate' => array('deadwood_validate_out_dir'),
  );

  return system_settings_form($form);
}

/**
 * Implementation of settings form sumission.
 *
 * Rename module input and output directories based on user settings.
 */
function deadwood_settings_submit($form, &$form_state) {
  $values = $form_state['values'];

  $cur = variable_get('deadwood_dir', DEADWOOD_IN);
  $new = $values['deadwood_dir'];
  if ($new != $cur) {
    $cur = file_directory_path() . '/' . $cur;
    $new = file_directory_path() . '/' . $new;
    rename($cur, $new);
  }

  $cur = variable_get('goodwood_dir', DEADWOOD_OUT);
  $new = $values['goodwood_dir'];
  if ($new != $cur) {
    $cur = file_directory_path() . '/' . $cur;
    $new = file_directory_path() . '/' . $new;
    rename($cur, $new);
  }
}

/**
 * Implementation of api settings form.
 */
function deadwood_api_settings() {
  $form = array();

  $form['conversions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default API conversions to apply'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE
  );

  $form['conversions']['table'] = deadwood_conversions_table('vid');

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
  );

  $form['reload'] = array(
    '#type' => 'submit',
    '#value' => t('Reload the API changes')
  );

  return $form;
}

/**
 * Implementation of api settings form sumission.
 *
 * Store default api conversions to apply based on user settings.
 */
function deadwood_api_settings_submit($form, &$form_state) {
  if ($form_state['values']['op'] == $form_state['values']['reload']) {
    module_load_include('install', 'deadwood', 'deadwood');
    deadwood_load_categories(FALSE);
    return;
  }

  $values = $form_state['values']['conversions']['table'];
  if (deadwood_categories_update($values)) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Display the module conversion form.
 */
function deadwood_conversions(&$form_state) {
  // Set default values.
  list($extensions, $directory, $conversions) = deadwood_conversions_defaults($form_state);

  $form = array();

  $form['extensions'] = array(
    '#type' => 'item',
    '#tree' => TRUE,
    '#theme' => array('deadwood_extensions_table')
  );
  // TODO Refactor this to a helper function?
  $types = array(
    'inc' => 'PHP code files',
    'info' => 'Info files used with module installation',
    'install' => 'PHP code files used with module installation, update and uninstallation',
    'module' => 'PHP code files',
    'php' => 'PHP code files',
    'profile' => 'PHP code files used with site installation',
    'test' => 'SimpleTest files',
    'theme' => 'PHP code files used with theming',
  );
  foreach ($types as $key => $type) {
    $row = array();
    $row['include'] = array(
      '#type' => 'checkbox',
      '#default_value' => $extensions[$key]
    );
    $row['title'] = array(
      '#type' => 'item',
      '#title' => t('Title'),
      '#value' => $key
    );
    $row['description'] = array(
      '#type' => 'item',
      '#title' => t('Description'),
      '#value' => $type
    );

    $form['extensions'][$key] = $row;
  }

  $options = array();
  $path = realpath(file_directory_path() . '/' . variable_get('deadwood_dir', DEADWOOD_IN));
  $dirs = deadwood_scan_directory($path);
  foreach ($dirs as $dir) {
    $options[$dir] = $dir;
  }
  if (!$dirs) {
    drupal_set_message(t('Please place modules to be converted in @path.', array('@path' => $path)), 'error');
  }

  $form['directory'] = array(
    '#title' => t('Directory'),
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => $directory,
    '#description' => t('Directory beneath the module input path (!path) in which to convert the selected files to 6.x code.', array('!path' => $path))
  );

  $form['conversions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conversions to apply'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE
  );

  $form['conversions']['table'] = deadwood_conversions_table('hook', $conversions);

  $form['convert'] = array(
    '#type' => 'submit',
    '#value' => t('Convert files')
  );

  // TODO Enable this button when code is ready.
  $form['format'] = array(
    '#type' => 'submit',
    '#value' => t('Format files'),
    '#disabled' => TRUE
  );

  return $form;
}

/**
 * Implementation of module conversion form validation.
 */
function deadwood_conversions_validate($form, &$form_state) {
  // Gather the file types to convert based on extension.
  $extensions = array();
  $values = $form_state['values']['extensions'];
  foreach ($values as $key => $value) {
    if ($value['include']) {
      $extensions[] = $key;
    }
  }
  if (!count($extensions)) {
    form_set_error('extensions', t('Please select at least one file extension.'));
  }

  // Gather the conversions to apply.
  $conversions = array();
  $values = $form_state['values']['conversions']['table'];
  foreach ($values as $key => $value) {
    if (strpos($key, '_missing') === 0) {
      continue;
    }
    if ($value['include']) {
      $conversions[] = $key;
    }
  }
  if ($form_state['values']['op'] != $form_state['values']['format'] && !count($conversions)) {
    form_set_error('conversions', t('Please select at least one conversion to apply.'));
  }
}

/**
 * Execute the module conversion code.
 */
function deadwood_conversions_submit($form, &$form_state) {
  // Rebuild form with user selections.
  $form_state['rebuild'] = TRUE;

  // Define the input and output directories.
  $dirname = file_directory_path() . '/' . variable_get('deadwood_dir', DEADWOOD_IN) . '/' . $form_state['values']['directory'];
  $newdirname = file_directory_path() . '/' . variable_get('goodwood_dir', DEADWOOD_OUT) . '/' . $form_state['values']['directory'];

  $params = array();

  // Gather the file types to convert based on extension.
  $extensions = array();
  $values = $form_state['values']['extensions'];
  foreach ($values as $key => $value) {
    if ($value['include']) {
      $extensions[] = $key;
    }
  }
  $params['extensions'] = $extensions;

  // Gather the conversions to apply.
  $conversions = array();
  $values = $form_state['values']['conversions']['table'];
  foreach ($values as $key => $value) {
    if (strpos($key, '_missing') === 0) {
      continue;
    }
    if ($value['include']) {
      $conversions[] = $key;
    }
  }
  $params['conversions'] = $conversions;

  // Apply format functions.
  if ($form_state['values']['op'] == $form_state['values']['format']) {
    module_load_include('inc', 'deadwood', 'deadwood.format');
    $params['conversions'] = array('format');
    deadwood_convert_dir($dirname, $newdirname, $params);
    drupal_set_message(t('Module format code was run.'));
  }
  // Apply conversion functions.
  else {
    deadwood_convert_dir($dirname, $newdirname, $params);
    drupal_set_message(t('Module conversion code was run.'));
  }
}

/**
 * Set the default file extensions to convert.
 *
 * @return array of default values.
 */
function deadwood_conversions_defaults($form_state) {
  // Set defaults when form is first loaded.
  $extensions = array(
    'inc' => TRUE,
    'info' => TRUE,
    'install' => TRUE,
    'module' => TRUE,
    'php' => FALSE,
    'profile' => FALSE,
    'test' => FALSE,
    'theme' => FALSE,
  );
  $directory = 'example';
  $conversions = array(); // If not set below, will be set in deadwood_conversions_table.

  // Set defaults from submitted values.
  if (isset($form_state['values'])) {
    if (isset($form_state['values']['extensions'])) {
      $values = $form_state['values']['extensions'];
      foreach ($values as $key => $value) {
        $extensions[$key] = $value['include'];
      }
    }
    if (isset($form_state['values']['directory'])) {
      $directory = $form_state['values']['directory'];
    }
    if (isset($form_state['values']['conversions']['table'])) {
      $values = $form_state['values']['conversions']['table'];
      foreach ($values as $key => $value) {
        $conversions[$key] = $value['include'];
      }
    }
  }

  return array($extensions, $directory, $conversions);
}

/**
 * Theme the conversion file extension form.
 *
 * @return HTML output.
 */
function theme_deadwood_extensions_table($form) {
  $select_header = theme('table_select_header_cell');
  $header = array($select_header, t('Extension'), t('Description'));
  $rows = array();
  foreach (element_children($form) as $key) {
    $task = &$form[$key];

    $row = array();
    $row[] = drupal_render($task['include']);
    $row[] = '<strong><label for="'. $task['include']['#id'] .'">'. $task['title']['#value'] .'</label></strong>';
    $row[] = $task['description']['#value'];

    $rows[] = $row;
  }

  return theme('table', $header, $rows);
}

/**
 * The deadwood conversions form.
 */
function deadwood_conversions_table($index = 'hook', $includes = array()) {
  $form = array();

  $form = array(
    '#type' => 'item',
    '#theme' => array('deadwood_conversions_table'),
  );

  $categories = deadwood_categories_load();
  foreach ($categories as $category) {
    $row = array();
    $row['include'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($includes[$category->hook]) ? $includes[$category->hook] : $category->include
    );
    $row['title'] = array(
      '#type' => 'item',
      '#title' => t('Title'),
      '#value' => l($category->title, 'node/' . $category->nid . '/edit')
    );
    $row['status'] = array(
      '#type' => 'item',
      '#title' => t('Status'),
      '#value' => deadwood_get_code_availability_status($category->code_status),
      '#class' => deadwood_get_code_availability_class($category->code_status)
    );

    $form[$category->$index] = $row;
  }

  return $form;
}

/**
 * Theme the conversion execution form.
 *
 * @return HTML output.
 */
function theme_deadwood_conversions_table($form) {
  $select_header = theme('table_select_header_cell');
  $category_header = array('data' => t('Category'), 'style' => 'width: 70%');
  $header = array($select_header, $category_header, t('Conversion Code'));
  $rows = array();
  foreach (element_children($form) as $key) {
    $task = &$form[$key];

    $row = array();
    $row[] = drupal_render($task['include']);
    $row[] = $task['title']['#value'];
    $row[] = array('header' => '', 'data' => $task['status']['#value']);

    $rows[] = array('data' => $row, 'class' => $task['status']['#class']);
  }
  $attributes = array('class' => 'deadwood-conversions');
  drupal_add_css(drupal_get_path('module', 'deadwood') .'/deadwood.css', 'module');

  return theme('table', $header, $rows, $attributes);
}

/**
 * Load the module conversion categories.
 *
 * @return array List of task node objects.
 */
function deadwood_categories_load() {
  $sql = 'SELECT c.nid, c.vid, weight, include, code_status, hook, title, body
          FROM {dw_category} c
          JOIN {node_revisions} n ON n.vid = c.vid
          ORDER BY weight';
  $result = db_query($sql);

  $tasks = array();
  while ($task = db_fetch_object($result)) {
    $tasks[] = $task;
  }
  return $tasks;
}

/**
 * Update a module conversion category.
 *
 * @param array $values Array indexed by vid with a value of $value['include'].
 * @return TRUE if all updates succeded.
 */
function deadwood_categories_update($values = array()) {
  if (!isset($values) || !is_array($values)) {
    return;
  }

  $success = TRUE;
  $sql = 'UPDATE {dw_category} SET include = %d WHERE vid = %d';
  foreach ($values as $vid => $include) {
    if (!db_query($sql, $include['include'], $vid)) {
      drupal_set_message(t('Update failed for category with vid = @vid', array('@vid' => $vid)), 'error');
      $success = FALSE;
    }
  }
  return $success;
}

// TODO Move these node hooks to a category.inc file when we implement dw_items
// node hooks in an item.inc file.

/**
 * Implementation of hook_form().
 *
 * This hook displays the form necessary to create/edit the deadwood category.
 */
function deadwood_form(&$node, $form_state) {
  $type = node_get_types('type', $node);
//  $editing = isset($node->nid);

  // Eliminate warnings about undefined properties when a node is created.
  if (!isset($node->nid)) {
    $node->weight = '';
    $node->include = 1;
    $node->code_status = 0;
    $node->hook = '';
  }

  $form = array();

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => check_plain($type->title_label),
    '#required' => TRUE,
    '#default_value' => $node->title,
  );
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => check_plain($type->body_label),
    '#required' => FALSE,
    '#default_value' => $node->body,
  );

  $form['body_filter']['format'] = filter_form($node->format);

  $form['weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#required' => TRUE,
    '#default_value' => $node->weight,
    '#description' => t('Weight for sorting when viewing a list of categories.'),
    '#maxlength' => 5,
    '#size' => 2,
    '#attributes' => array('style' => 'width: auto;'), // Otherwise set to 90% by Drupal style sheet
  );
  $form['include'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include'),
    '#default_value' => $node->include,
    '#description' => t('Check the box to include this category in the set of default conversions applied.'),
  );
  $form['code_status'] = array(
    '#type' => 'select',
    '#title' => t('Code conversion availability status'),
    '#default_value' => $node->code_status,
    '#options' => deadwood_get_code_statuses(),
    '#description' => t('Code conversion availability status.'),
  );
  $form['hook'] = array(
    '#type' => 'textfield',
    '#title' => t('Hook'),
    '#required' => TRUE,
    '#default_value' => $node->hook,
    '#description' => t('Name of the hook function to be called to handle conversions in this category. Example: enter "example" to call the function "deadwood_convert_example."'),
  );

  return $form;
}

/**
 * Implementation of hook_load().
 *
 * Load the deadwood category-specific data into the node object.
 */
function deadwood_load($node) {
  $category = db_fetch_object(db_query('SELECT * FROM {dw_category} WHERE vid = %d', $node->vid));

  return $category;
}

/**
 * Implementation of hook_validate().
 */
function deadwood_validate($node, &$form) {
  // Should we require the weight to be unique or the hook to be defined?
}

/**
 * Implementation of hook_insert().
 *
 * This is called upon node creation.
 */
function deadwood_insert($node) {
  db_query("INSERT INTO {dw_category} (nid, vid, weight, include, code_status, hook) VALUES (%d, %d, %d, %d, %d, '%s')", $node->nid, $node->vid, $node->weight, $node->include, $node->code_status, $node->hook);
}

/**
 * Implementation of hook_update().
 *
 * This is called upon node editing.
 */
function deadwood_update($node) {
  db_query("UPDATE {dw_category} SET weight = %d, include = %d, code_status = %d, hook = '%s' WHERE vid = %d", $node->weight, $node->include, $node->code_status, $node->hook, $node->vid);
}

/**
 * Implementation of hook_delete().
 */
function deadwood_delete(&$node) {
  // TODO Should the node parameter be passed by reference?
  db_query("DELETE FROM {dw_item} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {dw_category} WHERE nid = %d", $node->nid);
}

/**
 * Implementation of hook_view().
 */
function deadwood_view($node, $teaser = FALSE, $page = FALSE) {
//  drupal_add_css(drupal_get_path('module', 'deadwood') .'/deadwood.css', 'module');

  $node = node_prepare($node, $teaser);

  $statuses = deadwood_get_code_statuses();
  $node->content['include'] = array(
    '#type' => 'item',
    '#value' => t('Include this category in the set of default conversions applied: !include', array('!include' => $node->include ? 'True' : 'False')),
    '#weight' => 1,
  );
  $node->content['code_status'] = array(
    '#type' => 'item',
    '#value' => t('Code conversion availability status: !status', array('!status' => $statuses[$node->code_status])),
    '#weight' => 2,
  );
  $node->content['hook'] = array(
    '#type' => 'item',
    '#value' => t('Function hook: deadwood_convert_!hook', array('!hook' => $node->hook)),
    '#weight' => 3,
  );

  return $node;
}

/**
 * Implementation of hook_form_alter().
 */
function deadwood_form_alter(&$form, &$form_state, $form_id) {

}
