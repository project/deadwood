
AUTHOR
------
Jim Berry ("solotandem", http://drupal.org/user/240748)

PROJECT PAGE
------------
If you need more information, have an issue, or feature request please
visit the project page at: http://drupal.org/project/deadwood.

DESCRIPTION
-----------
The purpose of this module is to automate as much as possible the task of
updating a contributed module for Drupal API changes, and thereby simplify the
task of porting contributed modules shortly after a new Drupal release.

ONLINE CONVERSION
-----------------
Instead of downloading and installing Deadwood on your server, you may take
advantage of the free service offered at http://boombatower.com/tools/deadwood.
From this page, simply upload your module code (in a tar.gz file) and download
the converted module.

LOCAL CONVERSION
----------------
Other than installing Deadwood, before you can convert a module, you need to
upload your module's files to a directory beneath the "module input directory"
defined in this module's help page at admin/help/deadwood.

After installing the module, view the page admin/help/deadwood for a complete
description of the process.
