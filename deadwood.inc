<?php

/**
 * @file
 * Generate version upgrade code from 5.x to 6.x.
 *
 * Copyright 2008 by Jim Berry ("solotandem", http://drupal.org/user/240748)
 */

/**
 * The default directory to store modules to be converted.
 * Relative to file_directory_path().
 */
define('DEADWOOD_IN', 'deadwood');

/**
 * The default directory to store converted modules.
 * Relative to file_directory_path().
 */
define('DEADWOOD_OUT', 'goodwood');

/**
 * The values for the availability status of conversion code for a category.
 * NOTE: This should be for an item not a category.
 */
define('DEADWOOD_CONVERSION_CODE_NOT_AVAILABLE', 0);
define('DEADWOOD_CONVERSION_CODE_AVAILABLE', 1);
define('DEADWOOD_CONVERSION_CODE_PARTIALLY_AVAILABLE', 2);
define('DEADWOOD_CONVERSION_CODE_REQUIRES_MANUAL', 3);
define('DEADWOOD_CONVERSION_CODE_NOTIFY_ONLY', 4);
define('DEADWOOD_CONVERSION_CODE_NOT_NEEDED', 5);

/**
 * Pass a string through t() and wrap the result in html entity <p>.
 */
function tp($string, $args = array()) {
  return '<p>' . t($string, $args) . '</p>';
}

/**
 * Convert a category's code availability status (integer) to a string.
 *
 * @param integer $status Availability status code.
 * @return string Representation of availability status code.
 */
function deadwood_get_code_availability_status($status) {
  switch ($status) {
    case DEADWOOD_CONVERSION_CODE_AVAILABLE:
      return t('Available');
    case DEADWOOD_CONVERSION_CODE_PARTIALLY_AVAILABLE:
      return t('Partially available');
    case DEADWOOD_CONVERSION_CODE_REQUIRES_MANUAL:
      return t('Manual intervention');
    case DEADWOOD_CONVERSION_CODE_NOTIFY_ONLY:
      return t('Notification only');
    case DEADWOOD_CONVERSION_CODE_NOT_NEEDED:
      return t('Not needed');
    default:
      return t('Not available');
  }
}

/**
 * Convert a category's code availability status (integer) to a string.
 *
 * @param integer $status Availability status code.
 * @return string class representation of availability status code.
 */
function deadwood_get_code_availability_class($status) {
  switch ($status) {
    case DEADWOOD_CONVERSION_CODE_AVAILABLE:
      return 'available';
    case DEADWOOD_CONVERSION_CODE_PARTIALLY_AVAILABLE:
      return 'available';
    case DEADWOOD_CONVERSION_CODE_REQUIRES_MANUAL:
      return 'notify';
    case DEADWOOD_CONVERSION_CODE_NOTIFY_ONLY:
      return 'notify';
    case DEADWOOD_CONVERSION_CODE_NOT_NEEDED:
      return 'notify';
    default:
      return 'missing';
  }
}

/**
 * Get conversion code availability status values.
 *
 * @return array of statuses.
 */
function deadwood_get_code_statuses() {
  return array(
    '0' => 'Not available',
    '1' => 'Available',
    '2' => 'Partially available',
    '3' => 'Requires manual',
    '4' => 'Notify only',
    '5' => 'Not needed'
  );
}

/**
 * Scan a specified directory and find all first-level directories beneath it.
 *
 * TODO Replace this with a call to file_scan_directory in include/files.inc.
 *
 * @param string $path Directory path.
 * @return Array of directory names.
 */
function deadwood_scan_directory($path) {
  static $ignore = array('.', '..', '.svn');
  $dirs = array();

  $path = $path .'/';
  $files = scandir($path);
  foreach ($files as $file) {
    $file_path = $path . $file;
    if (is_dir($file_path) && !in_array(basename($file_path), $ignore)) {
      $dirs[] = $file;
    }
  }
  return $dirs;
}

/**
 * Remove all files from specified directory and optionally remove the directory.
 *
 * @param string $path Directory path.
 */
function deadwood_clean_directory($path, $remove_me = FALSE) {
  $path = $path .'/';
  $files = scandir($path);
  foreach ($files as $file) {
    if ($file != '.' && $file != '..') {
      $file_path = $path . $file;
      if (is_dir($file_path)) {
        deadwood_clean_directory($file_path, TRUE);
      }
      else {
        file_delete($file_path);
      }
    }
  }
  if ($remove_me) {
    rmdir($path);
  }
}

/**
 * Print debug information if debug flag is on.
 *
 * @param mixed $text
 *   A string or array to print.
 */
function deadwood_debug_print($text) {
  global $debug;

  if (!$debug) {
    return;
  }
  if (is_array($text) || is_object($text)) {
    file_put_contents(deadwood_log_path(), print_r($text, 1), FILE_APPEND);
  }
  else {
    file_put_contents(deadwood_log_path(), $text . "\n", FILE_APPEND);
  }
}

/**
 * Return path to log file.
 *
 * @return string
 *   Path to file.
 */
function deadwood_log_path() {
  static $path = '';

  if (!$path) {
    $path = file_directory_path() . '/' . DEADWOOD_IN . '/debug.txt';
  }
  return $path;
}
